import { shallowMount } from '@vue/test-utils'
import Homepage from '@/components/Homepage.vue'

describe('Homepage.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(Homepage, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
