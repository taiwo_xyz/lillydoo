// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('Discover our skin friendliness')
    cy.get('input').should('be.enabled').and('have.attr', 'value')  
  })
})
